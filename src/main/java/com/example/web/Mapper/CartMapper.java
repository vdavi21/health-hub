package com.example.web.Mapper;

import com.example.web.Dto.CartDto;
import com.example.web.Dto.ProductDto;
import com.example.web.models.Cart;
import com.example.web.models.Product;
import com.example.web.service.ProductService;

public class CartMapper {

    public static Cart mapToCart(CartDto cartDto,Product product) {
        Cart cart = Cart.builder()
                .id(cartDto.getId())
                .quantity(cartDto.getQuantity())
                .product(product)
                .createdBy(cartDto.getCreatedBy())
                .build();
        return cart;
    }
    public static CartDto mapToCartDto(Cart cart) {
        CartDto cartDto = CartDto.builder()
                .Product_id(cart.getProduct().getId())
                .product_name(cart.getProduct().getName())
                .quantity(cart.getQuantity())
                .createdBy(cart.getCreatedBy())
                .id(cart.getId())
                .total_Amount(cart.getQuantity() * cart.getProduct().getPrice())
                .build();
        return cartDto;
    }
}
