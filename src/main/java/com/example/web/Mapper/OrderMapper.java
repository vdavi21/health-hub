package com.example.web.Mapper;

import com.example.web.Dto.OrderDto;
import com.example.web.models.Order;

public class OrderMapper {
    public static OrderDto OrderToOrderDto(Order order){
        OrderDto orderDto = OrderDto.builder()
                .id(order.getId())
                .total_amount(order.getTotal_amount())
                .createdOn(order.getCreatedOn())
                .quantity(order.getQuantity())
                .createdBy(order.getCreatedBy())
                .build();
        return orderDto;
    }

    public static Order OrderDtoToOrder(OrderDto orderDto){
        Order order = Order.builder()
                .id(orderDto.getId())
                .total_amount(orderDto.getTotal_amount())
                .createdOn(orderDto.getCreatedOn())
                .quantity(orderDto.getQuantity())
                .createdBy(orderDto.getCreatedBy())
                .build();
        return order;
    }
}
