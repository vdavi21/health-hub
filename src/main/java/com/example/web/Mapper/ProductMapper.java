package com.example.web.Mapper;

import com.example.web.Dto.ProductDto;
import com.example.web.models.Product;

public class ProductMapper {
    public static Product mapToProduct(ProductDto productDto) {
        Product product = Product.builder()
                .id(productDto.getId())
                .name(productDto.getName())
                .description(productDto.getDescription())
                .photoUrl(productDto.getPhotoUrl())
                .createdBy(productDto.getCreatedBy())
                .price(productDto.getPrice())
                .build();
        return product;
    }

    public static ProductDto mapToProductDto(Product product) {
        ProductDto productDto = ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .photoUrl(product.getPhotoUrl())
                .createdBy(product.getCreatedBy())
                .price(product.getPrice())
                .build();
        return productDto;
    }
}
