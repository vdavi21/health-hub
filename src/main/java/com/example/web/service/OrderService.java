package com.example.web.service;

import com.example.web.Dto.OrderDto;
import com.example.web.models.Order;

import java.util.List;

public interface OrderService {
    void save(Order order);

    List<OrderDto> getOrderList();

    void clearHistory();
}
