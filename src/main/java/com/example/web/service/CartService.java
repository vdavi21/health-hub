package com.example.web.service;

import com.example.web.Dto.CartDto;
import com.example.web.Dto.ProductDto;
import com.example.web.models.Order;

import java.util.List;

public interface CartService {
    void save(ProductDto product);

    List<CartDto> getCartList();

    void delete(Long cart_id);

    Order checkOut();
}
