package com.example.web.service.impl;

import com.example.web.Dto.ProductDto;
import com.example.web.Security.SecurityUtil;
import com.example.web.models.Product;
import com.example.web.models.UserEntity;
import com.example.web.repository.ProductRepository;
import com.example.web.repository.UserRepository;
import com.example.web.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static com.example.web.Mapper.ProductMapper.mapToProduct;
import static com.example.web.Mapper.ProductMapper.mapToProductDto;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    private ProductRepository productRepository;
    private UserRepository userRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository,UserRepository userRepository) {
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<ProductDto> findAll() {
        List<Product> products = productRepository.findAll();
        return products.stream().map((Product) -> mapToProductDto(Product)).collect(Collectors.toList());
    }

    @Override
    public Product save(ProductDto productDto) {
        String username = SecurityUtil.getSessionUser();
        System.out.println(username);
        UserEntity userEntity = userRepository.findByEmail(username);
        Product product = mapToProduct(productDto);
        product.setCreatedBy(userEntity);
        return productRepository.save(product);
    }

    @Override
    public ProductDto findProductById(long productId) {
        Product product = productRepository.findById(productId).get();
        return mapToProductDto(product);
    }

    @Override
    public void updateProduct(ProductDto productDto) {
        String username = SecurityUtil.getSessionUser();
        UserEntity userEntity = userRepository.findByEmail(username);
        Product product = mapToProduct(productDto);
        product.setCreatedBy(userEntity);
        productRepository.save(product);
    }

    @Override
    public void delete(Long productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public List<ProductDto> searchProduct(String query) {
        List<Product> products = productRepository.searchProduct(query);
        return products.stream().map((Product) -> mapToProductDto(Product)).collect(Collectors.toList());
    }
}
