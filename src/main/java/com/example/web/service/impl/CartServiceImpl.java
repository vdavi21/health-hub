package com.example.web.service.impl;

import com.example.web.Dto.CartDto;
import com.example.web.Dto.ProductDto;
import com.example.web.Security.SecurityUtil;
import com.example.web.models.Cart;
import com.example.web.models.Order;
import com.example.web.models.Product;
import com.example.web.models.UserEntity;
import com.example.web.repository.CartRepository;
import com.example.web.repository.UserRepository;
import com.example.web.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.example.web.Mapper.CartMapper.mapToCartDto;
import static com.example.web.Mapper.ProductMapper.mapToProduct;
import static com.example.web.Mapper.ProductMapper.mapToProductDto;

@Service
public class CartServiceImpl implements CartService {
    private CartRepository cartRepository;
    private UserRepository userRepostiory;

    @Autowired
    public CartServiceImpl(CartRepository cartRepository, UserRepository userRepository) {
        this.cartRepository = cartRepository;
        this.userRepostiory = userRepository;
    }

    @Override
    public void save(ProductDto productDto) {
        Product product = mapToProduct(productDto);
        Optional<Cart> existingCart = cartRepository.findByProduct(product);
        String username = SecurityUtil.getSessionUser();
        UserEntity userEntity = userRepostiory.findByEmail(username);
        if(existingCart.isPresent()){
            Cart cart = existingCart.get();
            cart.setQuantity(cart.getQuantity() + 1);
            cart.setCreatedBy(userEntity);
            cartRepository.save(cart);
        }else{
            Cart cart = new Cart();
            cart.setProduct(product);
            cart.setQuantity(1);
            cart.setCreatedBy(userEntity);
            cartRepository.save(cart);
        }
    }

    @Override
    public List<CartDto> getCartList() {
        String user = SecurityUtil.getSessionUser();
        UserEntity entity = userRepostiory.findByEmail(user);
        List<Cart> carts = cartRepository.findByCreatedBy(entity);
        return carts.stream().map((Cart) -> mapToCartDto(Cart)).collect(Collectors.toList());
    }

    @Override
    public void delete(Long cart_id) {
        cartRepository.deleteById(cart_id);
    }

    @Override
    public Order checkOut() {
        String username = SecurityUtil.getSessionUser();
        UserEntity userEntity = userRepostiory.findByEmail(username);
        List<Cart> carts = cartRepository.findByCreatedBy(userEntity);
        float sum = 0;
        int quantity = 0;
        for(int i=0;i<carts.size();i++){
            sum += carts.get(i).getQuantity() * carts.get(i).getProduct().getPrice();
            quantity += carts.get(i).getQuantity();
        }
        cartRepository.deleteByCreatedBy(userEntity);
        Order order = new Order();
        order.setTotal_amount(sum);
        order.setQuantity(quantity);
        order.setCreatedBy(userEntity);
        return order;
    }
}
