package com.example.web.service.impl;

import com.example.web.Dto.OrderDto;
import com.example.web.Security.SecurityUtil;
import com.example.web.models.Order;
import com.example.web.models.UserEntity;
import com.example.web.repository.OrderRepository;
import com.example.web.repository.UserRepository;
import com.example.web.service.OrderService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.web.Mapper.OrderMapper.OrderToOrderDto;

@Service
public class OrderServiceImpl implements OrderService {
    private OrderRepository orderRepository;
    private UserRepository userRepository;

    public OrderServiceImpl(OrderRepository orderRepository,UserRepository userRepository) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void save(Order order) {
        orderRepository.save(order);
    }

    @Override
    public List<OrderDto> getOrderList() {
        String user = SecurityUtil.getSessionUser();
        UserEntity entity = userRepository.findByEmail(user);
        List<Order> list = orderRepository.findByCreatedBy(entity);
        return list.stream().map((Order) -> OrderToOrderDto(Order)).collect(Collectors.toList());
    }

    @Override
    public void clearHistory() {
        String email = SecurityUtil.getSessionUser();
        UserEntity user = userRepository.findByEmail(email);
        orderRepository.deleteByCreatedBy(user);
    }
}
