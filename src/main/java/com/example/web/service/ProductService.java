package com.example.web.service;

import com.example.web.Dto.ProductDto;
import com.example.web.models.Product;

import java.util.List;

public interface ProductService {
    List<ProductDto> findAll();

    Product save(ProductDto productDto);

    ProductDto findProductById(long productId);

    void updateProduct(ProductDto product);

    void delete(Long productId);

    List<ProductDto> searchProduct(String query);
}
