package com.example.web.repository;

import com.example.web.models.Cart;
import com.example.web.models.Product;
import com.example.web.models.UserEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart,Long> {

    Optional<Cart> findByProduct(Product product);

    List<Cart> findByCreatedBy(UserEntity user);

    @Modifying
    @Transactional
    @Query("DELETE FROM Cart c WHERE c.createdBy = :user")
    void deleteByCreatedBy(@Param("user") UserEntity userEntity);
}
