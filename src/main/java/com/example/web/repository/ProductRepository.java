package com.example.web.repository;

import com.example.web.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Long> {
    @Query("SELECT c from Product c WHERE c.name LIKE CONCAT('%', :query, '%')")
    List<Product> searchProduct(String query);
}
