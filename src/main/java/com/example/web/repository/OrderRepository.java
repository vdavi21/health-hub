package com.example.web.repository;


import com.example.web.models.Order;
import com.example.web.models.UserEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order,Long> {

    List<Order> findByCreatedBy(UserEntity entity);

    @Modifying
    @Transactional
    @Query("DELETE FROM Order c WHERE c.createdBy = :user")
    void deleteByCreatedBy(@Param("user") UserEntity user);
}
