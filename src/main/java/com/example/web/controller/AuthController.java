package com.example.web.controller;


import com.example.web.Dto.RegistrationDto;
import com.example.web.models.UserEntity;
import com.example.web.service.UserService;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;



@Validated
@Controller
public class AuthController {
    UserService userService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AuthController(UserService userService,PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/")
    public String LoginPage(Model model){
        return "index";
    }

    @GetMapping("/register")
    public String getRegister(Model model){
        RegistrationDto user = new RegistrationDto();
        model.addAttribute("user", user);
        return "Register";
    }

    @PostMapping("/register/save")
    public String RegisterSave(@Valid @ModelAttribute("user") RegistrationDto user,
                               BindingResult result,Model model){
        UserEntity existingUserEmail = userService.findByEmail(user.getEmail());
        if(existingUserEmail != null && existingUserEmail.getEmail() != null && !existingUserEmail.getEmail().isEmpty()){
            return "redirect:/register?fail";
        }
        UserEntity existingUserUsername = userService.findByUsername(user.getUsername());
        if(existingUserUsername != null && existingUserUsername.getUsername() != null && !existingUserUsername.getUsername().isEmpty()){
            return "redirect:/register?fail";
        }
        if (result.hasErrors()) {
            model.addAttribute("user", user);
            return "Register";
        }
        userService.saveUser(user);
        return "redirect:/home?success";
    }
}
