package com.example.web.controller;

import com.example.web.Dto.ProductDto;
import com.example.web.Security.SecurityUtil;
import com.example.web.models.Product;
import com.example.web.models.UserEntity;
import com.example.web.service.ProductService;

import com.example.web.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@Controller
public class ProductController {
    private ProductService productService;
    private UserService userService;

    @Autowired
    public ProductController(ProductService productService, UserService userService) {
        this.productService = productService;
        this.userService = userService;
    }

    @GetMapping("/home")
    public String listProducts(Model model){
        List<ProductDto> products = productService.findAll();
        UserEntity user = new UserEntity();
        String email = SecurityUtil.getSessionUser();
        if(email != null){
            user = userService.findByEmail(email);
            model.addAttribute("user", user);
        }
        model.addAttribute("user",user);
        model.addAttribute("products", products);
        return "Home";
    }

    @GetMapping("/cart/new")
    public String createProductForm(Model model){
        Product product = new Product();
        model.addAttribute("product", product);
        return "AddProduct";
    }

    @PostMapping("/cart/new")
    public String saveProduct(@Valid @ModelAttribute("product") ProductDto product, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "redirect:/cart/new";
        }
        productService.save(product);
        return "redirect:/home";
    }


    @GetMapping("/home/{productId}/edit")
    public String editProductForm(@PathVariable("productId") Long productId,Model model){
        ProductDto product = productService.findProductById(productId);
        model.addAttribute("product", product);
        return "EditProduction";
    }

    @GetMapping("/home/{productId}/delete")
    public String deleteProduct(@PathVariable("productId") Long productId, Model model){
        productService.delete(productId);
        return "redirect:/home";
    }

    @GetMapping("/home/search")
    public String searchProduct(@RequestParam(value = "query")String query, Model model){
        List<ProductDto> products = productService.searchProduct(query);
        model.addAttribute("products", products);
        return "Home";
    }
    @PostMapping("/home/{productId}/edit")
    public String editProduct(@PathVariable("productId") Long productId,
                              @Valid @ModelAttribute("product") ProductDto product,
                              BindingResult result){
        if(result.hasErrors()){
            return "EditProduction";
        }
        product.setId(productId);
        productService.updateProduct(product);
        return "redirect:/home";
    }
}
