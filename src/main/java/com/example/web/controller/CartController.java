package com.example.web.controller;

import com.example.web.Dto.CartDto;
import com.example.web.Dto.OrderDto;
import com.example.web.Dto.ProductDto;
import com.example.web.Security.SecurityUtil;
import com.example.web.models.Order;
import com.example.web.models.Product;
import com.example.web.models.UserEntity;
import com.example.web.repository.UserRepository;
import com.example.web.service.CartService;
import com.example.web.service.OrderService;
import com.example.web.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class CartController {
    private CartService cartService;

    private ProductService productService;
    private OrderService orderService;
    private UserRepository userRepository;

    public CartController(CartService cartService, ProductService productService,OrderService orderService,UserRepository userRepository) {
        this.cartService = cartService;
        this.productService = productService;
        this.orderService = orderService;
        this.userRepository = userRepository;
    }

    @GetMapping("/home/{productId}/addToCart")
    public String addProductToCart(@PathVariable("productId") Long productId){
        ProductDto product = productService.findProductById(productId);
        cartService.save(product);
        return "redirect:/home";
    }

    @GetMapping("/cart")
    public String listCart(Model model){
        List<CartDto> list = cartService.getCartList();
        List<OrderDto> listOrder = orderService.getOrderList();
        String email = SecurityUtil.getSessionUser();
        UserEntity user = userRepository.findByEmail(email);
        model.addAttribute("user", user);
        model.addAttribute("cartList", list);
        model.addAttribute("orderList",listOrder);
        return "Cart";
    }

    @GetMapping("/cart/{cart_id}/remove")
    public String deleteProductFromCart(@PathVariable("cart_id") Long cart_id){
        cartService.delete(cart_id);
        return "redirect:/cart";
    }

    @GetMapping("/cart/checkOut")
    public String checkOut(){
        Order order = cartService.checkOut();
        orderService.save(order);
        return "redirect:/cart";
    }
    @GetMapping("/cart/clearHistory")
    public String clearHistory(){
        orderService.clearHistory();
        return "redirect:/cart";
    }
}
