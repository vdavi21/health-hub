package com.example.web.Dto;

import com.example.web.models.UserEntity;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class OrderDto {
    private Long id;
    private float total_amount;
    private int quantity;
    private LocalDateTime createdOn;
    private UserEntity createdBy;
}
