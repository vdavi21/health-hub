package com.example.web.Dto;


import com.example.web.models.UserEntity;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;



@Data
@Builder
public class ProductDto {
    private Long id;
    @NotEmpty(message = "Product name should not be empty")
    private String name;
    @NotEmpty(message = "Product description should not be empty")
    private String description;
    @NotNull(message = "Product price should not be empty")
    private float price;
    @NotEmpty(message = "Product photoUrl should not be empty")
    private String photoUrl;
    private UserEntity createdBy;
}
