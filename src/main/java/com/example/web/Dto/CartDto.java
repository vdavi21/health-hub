package com.example.web.Dto;

import com.example.web.models.UserEntity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CartDto {
    private Long id;
    private Long Product_id;
    private Integer quantity;
    private String product_name;
    private float total_Amount;
    private UserEntity createdBy;
}
